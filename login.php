<?php
/**
 * Created by PhpStorm.
 * User: tommy
 * Date: 3/14/2018
 * Time: 1:34 PM
 */

include_once "header.php"
?>
<br>
<div class="container">
    <div class="row">
        <div class="col s8 offset-s2">
            <div class="card">
                <div class="card-content">
                    <div class="row">
                        <form id="loginForm" method="post" class="col s12" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="username" name="username" type="text" class="validate">
                                    <label for="username">Username</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="password" name="password" type="password" class="validate">
                                    <label for="password">Password</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 right-align">
                                    <!--<input id="submit" name="submit" type="submit" class="waves-effect waves-light btn" value="Login">-->
                                    <button class="waves-effect waves-light btn" type="submit" name="submit">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once "footer.php"
?>

<?php
// Login
if(isset($_POST['submit']))
{
    if (!empty($_POST['username']) && !empty($_POST['password']))
    {
        $username = $_POST['username'];
        $password = $_POST['password'];

        // Checks student table first
        $stmt = $conn->prepare('SELECT * FROM `account` WHERE `Username` = ?');

        $stmt->bind_param('s', $username);

        // execute query
        $stmt->execute();

        // Get the result
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();

        if ($result->num_rows === 1)
        {
            $role = $row['AccountType'];

            if ($password != $row['Password']){
                echo "<script>alert('Wrong username / password. Please try again.');";
                echo "window.location.replace('login.php');</script>";
                return false;
            }
            else{
                $_SESSION['user'] = $username;
                $_SESSION['role'] = $role;
                echo "<script>window.location.replace('index.php');</script>";
            }
        }
    }
    else
    {
        echo "<script>alert('Please fill in all empty fields.');";
        echo "window.location.replace('login.php');</script>";
    }
}

?>
