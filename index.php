<?php
/**
 * Created by PhpStorm.
 * User: TomYaMee
 * Date: 2/6/2018
 * Time: 6:21 PM
 */

    include_once "header.php"
?>

    <div class="parallax-container">
        <div class="parallax"><img class="responsive-img" src="images/MaerskLanding.jpg"></div>
        <div class="container">
            <br>
            <h1 class="header center red-text">Maersk</h1>

            <br>
            <div class="row center">
                <div class="col s12 red-text">The leading shipment company you can trust</div>
            </div>
        </div>
    </div>

    <br>
    <div class="container">
         <div class="row">
             <div class="col s12 m6 center">
                 <h5>Vessel Management</h5>
                 View and Manage Vessel related to Maersk directly on the webpage
             </div>

             <div class="col s12 m6 center">
                 <h5>Shipment Management</h5>
                 View and Manage Shipment related to Maersk directly on the webpage
             </div>
         </div>
    </div>

<?php
    include_once "footer.php"
?>