<?php
/**
 * Created by PhpStorm.
 * User: tommy
 * Date: 3/14/2018
 * Time: 1:34 PM
 */

include "logincheck.php";
include_once "header.php"
?>

    <div class="container">

        <br>
        <div class="row">
            <div class="col s3 offset-s9">
                <a href="shipment-add.php" class="waves-effect waves-light btn"><i class="material-icons left">add</i>Create</a>
            </div>
        </div>

        <table class="responsive-table highlight">
            <thead>
            <tr>
                <th>ID</th>
                <th>Customer Name</th>
                <th>Item</th>
                <th>Vessel</th>
                <th>Shipment Date</th>
                <th>Action</th>
            </tr>
            </thead>

            <tbody>
                <?php
                $sql2= "SELECT * FROM shipment";
                $result = $conn->query($sql2);
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                        echo "<tr>";
                        echo "<td>" . $row["sid"] . "</td>";
                        echo "<td>" . $row["cname"] . "</td>";
                        echo "<td>" . $row["iname"] . "</td>";
                        echo "<td>" . $row["vname"] . "</td>";
                        echo "<td>" . $row["date"] . "</td>";
                        echo '<td> <a style="color: black" href="shipment-edit.php?id=' . $row["sid"] . '"><i class="material-icons">edit</i></a><a style="color: black" href="shipment-delete.php?id=' . $row["sid"] . '"> <i class="material-icons">delete</i></a> </td>';
                        echo "</tr>";
                    }
                };
                ?>
            </tbody>
        </table>
        <br>
        <br>
    </div>

<?php
include_once "footer.php"
?>