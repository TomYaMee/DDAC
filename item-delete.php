<?php
/**
 * Created by PhpStorm.
 * User: TomYaMee
 * Date: 4/4/2018
 * Time: 1:41 PM
 */

include "config.php";
$stmt = $conn->prepare('DELETE FROM `item` WHERE id = ?');
$stmt->bind_param('i', $_GET['id']);

// Execute query
$stmt->execute();

// Get the result
$result = $stmt->get_result();

echo "<script>alert('Item data deleted.'); window.location.replace('item.php');</script>";