<?php
/**
 * Created by PhpStorm.
 * User: TomYaMee
 * Date: 4/4/2018
 * Time: 1:55 PM
 */

include "config.php";
$stmt = $conn->prepare('DELETE FROM `vessel` WHERE vid = ?');
$stmt->bind_param('i', $_GET['id']);

// Execute query
$stmt->execute();

// Get the result
$result = $stmt->get_result();

echo "<script>alert('Vessel data deleted.'); window.location.replace('vessel.php');</script>";