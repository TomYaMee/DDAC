<?php
/**
 * Created by PhpStorm.
 * User: TomYaMee
 * Date: 4/4/2018
 * Time: 2:02 PM
 */
include_once "header.php"
?>

<br>
<div class="container">
    <div class="row">
        <div class="col s8 offset-s2">
            <div class="card">
                <div class="card-content">
                    <div class="row">
                        <form id="loginForm" method="post" class="col s12" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="name" name="name" type="text" class="validate">
                                    <label for="name">Agent Name</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="user" name="user" type="text" class="validate">
                                    <label for="user">Username</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="pass" name="pass" type="password" class="validate">
                                    <label for="pass">Password</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="mail" name="mail" type="email" class="validate">
                                    <label for="mail">Email</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 right-align">
                                    <!--<input id="submit" name="submit" type="submit" class="waves-effect waves-light btn" value="Login">-->
                                    <button class="waves-effect waves-light btn" type="submit" name="submit">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once "footer.php"
?>

<?php
// Login
if(isset($_POST['submit']))
{
    if (!empty($_POST['name']) && !empty($_POST['user']) && !empty($_POST['pass']) && !empty($_POST['mail']))
    {
        $name = $_POST['name'];
        $user = $_POST['user'];
        $pass = $_POST['pass'];
        $mail = $_POST['mail'];
        $agent = 'Agent';

        $stmt = $conn->prepare('Insert INTO agent (aname, auser, apass, aemail) VALUES (?, ?, ?, ?)');

        $stmt->bind_param('ssss', $name,$user, $pass, $mail);
        // execute query
        $stmt->execute();

        $stmt2 = $conn->prepare('Insert INTO account (Username, Password, AccountType) VALUES (?, ?, ?)');

        $stmt2->bind_param('sss',$user, $pass, $agent);
        $stmt2->execute();

        echo "<script>window.location.replace('agents.php');</script>";

    }
    else
    {
        echo "<script>alert('Please fill in all empty fields.');";
        echo "window.location.replace('agents.php');</script>";
    }
}

?>