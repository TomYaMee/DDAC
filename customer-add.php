<?php
/**
 * Created by PhpStorm.
 * User: TomYaMee
 * Date: 3/18/2018
 * Time: 12:53 PM
 */
include_once "header.php"
?>

<br>
<div class="container">
    <div class="row">
        <div class="col s8 offset-s2">
            <div class="card">
                <div class="card-content">
                    <div class="row">
                        <form id="loginForm" method="post" class="col s12" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="name" name="name" type="text" class="validate">
                                    <label for="name">Customer Name</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="address" name="address" type="text" class="validate">
                                    <label for="address">Address</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="contact" name="contact" type="text" class="validate">
                                    <label for="contact">Contact Number</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 right-align">
                                    <!--<input id="submit" name="submit" type="submit" class="waves-effect waves-light btn" value="Login">-->
                                    <button class="waves-effect waves-light btn" type="submit" name="submit">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once "footer.php"
?>

<?php
// Login
if(isset($_POST['submit']))
{
    if (!empty($_POST['name']) && !empty($_POST['address']) && !empty($_POST['contact']))
    {
        $name = $_POST['name'];
        $address = $_POST['address'];
        $contact = $_POST['contact'];

        $stmt = $conn->prepare('Insert INTO Customer (Name, Address, Contact) VALUES (?, ?, ?)');

        $stmt->bind_param('sss', $name,$address,$contact);

        // execute query
        $stmt->execute();

        echo "<script>window.location.replace('customer.php');</script>";

    }
    else
    {
        echo "<script>alert('Please fill in all empty fields.');";
        echo "window.location.replace('customer.php');</script>";
    }
}

?>
