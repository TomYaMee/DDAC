<?php
/**
 * Created by PhpStorm.
 * User: tommy
 * Date: 3/14/2018
 * Time: 1:34 PM
 */

include_once "header.php"
?>

    <div class="container">

        <br>
        <div class="row">
            <div class="col s3 offset-s9">
                <a href="vessel-add.php" class="waves-effect waves-light btn"><i class="material-icons left">add</i>Create</a>
            </div>
        </div>

        <table class="responsive-table highlight">
            <thead>
            <tr>
                <th>ID</th>
                <th>Vessel</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
            </thead>

            <tbody>
                <?php
                $sql2= "SELECT * FROM vessel";
                $result = $conn->query($sql2);
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                        echo "<tr>";
                        echo "<td>" . $row["vid"] . "</td>";
                        echo "<td>" . $row["vname"] . "</td>";
                        echo "<td>" . $row["vdesc"] . "</td>";
                        echo '<td> <a style="color: black" href="vessel-edit.php?id=' . $row["vid"] . '"><i class="material-icons">edit</i></a><a style="color: black" href="vessel-delete.php?id=' . $row["vid"] . '"> <i class="material-icons">delete</i></a> </td>';
                        echo "</tr>";
                    }
                };
                ?>
            </tbody>
        </table>
        <br>
        <br>
    </div>

<?php
include_once "footer.php"
?>