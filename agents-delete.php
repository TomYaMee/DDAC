<?php
/**
 * Created by PhpStorm.
 * User: TomYaMee
 * Date: 4/4/2018
 * Time: 2:02 PM
 */

include "config.php";
$stmt = $conn->prepare('DELETE FROM `agent` WHERE auser = ?');
$stmt->bind_param('s', $_GET['id']);

// Execute query
$stmt->execute();

$stmt2 = $conn->prepare('DELETE FROM `account` WHERE username = ?');
$stmt2->bind_param('s', $_GET['id']);

// Execute query
$stmt2->execute();

// Get the result
$result = $stmt->get_result();

echo "<script>alert('Agent data deleted.'); window.location.replace('agents.php');</script>";