<?php
/**
 * Created by PhpStorm.
 * User: TomYaMee
 * Date: 4/4/2018
 * Time: 2:02 PM
 */

include_once "header.php";

if(isset($_POST['submit']))
{
    if (!empty($_POST['name']) && !empty($_POST['user']) && !empty($_POST['pass']) && !empty($_POST['mail']))
    {
        $name = $_POST['name'];
        $user = $_POST['user'];
        $pass = $_POST['pass'];
        $email = $_POST['mail'];

        $stmt = $conn->prepare('UPDATE `agent` SET `aname`= ?,`auser`= ?,`apass`= ?, `aemail` = ? WHERE `auser` = ?');

        $stmt->bind_param('sssss', $name, $user, $pass, $email, $user);

        // execute query
        $stmt->execute();

        $stmt2 = $conn->prepare('UPDATE `account` SET `Username`= ?,`Password`= ? WHERE `Username` = ?');

        $stmt2->bind_param('sss', $user, $pass, $user);

        // execute query
        $stmt2->execute();

        echo "<script>alert('Update successfully');window.location.replace('agents.php');</script>";

    }
    else
    {
        echo "<script>alert('Please fill in all empty fields.');";
        echo "window.location.replace('agents.php');</script>";
    }
}
else if(isset($_GET['id']))
{
    $id = $_GET['id'];
    $stmt = $conn->prepare('SELECT * FROM `agent` WHERE `auser` = ?');

    $stmt->bind_param('s', $id);

    // execute query
    $stmt->execute();

    // Get the result
    $result = $stmt->get_result();
    $row = $result->fetch_assoc();

    if ($result->num_rows === 1)
    {
        $name = $row['aname'];
        $user = $row['auser'];
        $email = $row['aemail'];

    };
}
?>


    <br>
    <div class="container">
        <div class="row">
            <div class="col s8 offset-s2">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form id="loginForm" method="post" class="col s12" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                                <input type="hidden" name="id" id="id" value="<?php echo $id; ?>">

                                <div class="row">
                                    <div class="input-field col s12">
                                        <input value="<?php echo $name?>" id="name" name="name" type="text" class="validate">
                                        <label for="name">Agent Name</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input value="<?php echo $user?>" id="user" name="user" type="text" class="validate">
                                        <label for="user">Username</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="pass" name="pass" type="password" class="validate">
                                        <label for="pass">Password</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input value="<?php echo $email?>" id="mail" name="mail" type="text" class="validate">
                                        <label for="mail">Email</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12 right-align">
                                        <!--<input id="submit" name="submit" type="submit" class="waves-effect waves-light btn" value="Login">-->
                                        <button class="waves-effect waves-light btn" type="submit" name="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
include_once "footer.php"
?>