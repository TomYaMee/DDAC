<?php
/**
 * Created by PhpStorm.
 * User: TomYaMee
 * Date: 4/4/2018
 * Time: 2:36 PM
 */

include_once "header.php"
?>

    <br>
    <div class="container">
        <div class="row">
            <div class="col s8 offset-s2">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form id="loginForm" method="post" class="col s12" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <select name="name">
                                            <option value="0" disabled selected>Choose your option</option>

                                            <?php
                                                //http://www.w3schools.com/php/php_mysql_select.asp

                                                $stmt = $conn->prepare('SELECT Name, Address FROM `customer`');
                                                // execute query
                                                $stmt->execute();
                                                // Get the result
                                                $result = $stmt->get_result();

                                                if ($result->num_rows > 0) {
                                                    // output data of each row
                                                    while($row = $result->fetch_assoc()) {
                                                        echo "<option value=\"". $row["Name"]."\">". $row["Name"]." - ". $row["Address"] ."</option>";
                                                    }
                                                } else {
                                                    echo "<option value= \"0\"> 0 results</option>";}
                                            ?>
                                        </select>
                                        <label>Customer Name</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <select name="iname">
                                            <option value="0" disabled selected>Choose your option</option>

                                            <?php
                                            //http://www.w3schools.com/php/php_mysql_select.asp

                                            $stmt = $conn->prepare('SELECT itemname, itemdesc FROM `item`');
                                            // execute query
                                            $stmt->execute();
                                            // Get the result
                                            $result = $stmt->get_result();

                                            if ($result->num_rows > 0) {
                                                // output data of each row
                                                while($row = $result->fetch_assoc()) {
                                                    echo "<option value=\"". $row["itemname"]."\">". $row["itemname"]." - ". $row["itemdesc"] ."</option>";
                                                }
                                            } else {
                                                echo "<option value= \"0\"> 0 results</option>";}
                                            ?>
                                        </select>
                                        <label>Item Name</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <select name="vname">
                                            <option value="0" disabled selected>Choose your option</option>

                                            <?php
                                            //http://www.w3schools.com/php/php_mysql_select.asp

                                            $stmt = $conn->prepare('SELECT vname, vdesc FROM `vessel`');
                                            // execute query
                                            $stmt->execute();
                                            // Get the result
                                            $result = $stmt->get_result();

                                            if ($result->num_rows > 0) {
                                                // output data of each row
                                                while($row = $result->fetch_assoc()) {
                                                    echo "<option value=\"". $row["vname"]."\">". $row["vname"]." - ". $row["vdesc"] ."</option>";
                                                }
                                            } else {
                                                echo "<option value= \"0\"> 0 results</option>";}
                                            ?>
                                        </select>
                                        <label>Vessel Name</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="date" name="date" type="text" class="datepicker">
                                        <label for="date">Shipment Date</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12 right-align">
                                        <!--<input id="submit" name="submit" type="submit" class="waves-effect waves-light btn" value="Login">-->
                                        <button class="waves-effect waves-light btn" type="submit" name="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
include_once "footer.php"
?>

<?php
// Login
if(isset($_POST['submit']))
{
    if (!empty($_POST['name']) && !empty($_POST['iname']) && !empty($_POST['vname']) && !empty($_POST['date']))
    {
        $name = $_POST['name'];
        $iname = $_POST['iname'];
        $vessel = $_POST['vname'];
        $date = $_POST['date'];

        if ($name === 0 || $iname === 0 || $vessel === 0){
            echo "<script>alert('Please fill in all empty fields.'";
            echo "window.location.replace('shipment.php');</script>";
        }
        else {
            $stmt = $conn->prepare('Insert INTO shipment (cname, iname, vname, date) VALUES (?, ?, ?, ?)');

            $stmt->bind_param('ssss', $name, $iname, $vessel, $date);
            // execute query
            $stmt->execute();

            echo "<script>window.location.replace('shipment.php');</script>";
        }

    }
    else
    {
        echo "<script>alert('Please fill in all empty fields.'";
        echo "window.location.replace('shipment.php');</script>";
    }
}

?>