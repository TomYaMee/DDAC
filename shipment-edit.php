<?php
/**
 * Created by PhpStorm.
 * User: TomYaMee
 * Date: 4/4/2018
 * Time: 2:36 PM
 */

include_once "header.php";

if(isset($_POST['submit']))
{
    if (!empty($_POST['name']) && !empty($_POST['iname']) && !empty($_POST['vname']) && !empty($_POST['date']))
    {
        $name = $_POST['name'];
        $iname = $_POST['iname'];
        $vessel = $_POST['vname'];
        $date = $_POST['date'];
        $id = $_POST['sid'];

        $stmt = $conn->prepare('UPDATE `shipment` SET `cname`= ?,`iname`= ?, `vname` = ?, `date` = ? WHERE `sid` = ?');

        $stmt->bind_param('ssssi', $name,$iname, $vessel, $date, $id);

        // execute query
        $stmt->execute();

        echo "<script>alert('Update successfully');window.location.replace('shipment.php');</script>";

    }
    else
    {
        echo "<script>alert('Please fill in all empty fields.');";
        echo "window.location.replace('shipment.php');</script>";
    }
}
else if(isset($_GET['id']))
{
    $id = $_GET['id'];
    $stmt = $conn->prepare('SELECT * FROM `shipment` WHERE `sid` = ?');

    $stmt->bind_param('i', $id);

    // execute query
    $stmt->execute();

    // Get the result
    $result = $stmt->get_result();
    $row = $result->fetch_assoc();

    if ($result->num_rows === 1)
    {
        $sid = $row['sid'];
        $name = $row['cname'];
        $iname = $row['iname'];
        $vessel = $row['vname'];
        $date = $row['date'];

    };
}
?>

    <br>
    <div class="container">
        <div class="row">
            <div class="col s8 offset-s2">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form id="loginForm" method="post" class="col s12" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                                <input type="hidden" name="sid" id="sid" value="<?php echo $sid; ?>">

                                <div class="row">
                                    <div class="input-field col s12">
                                        <select name="name">

                                            <?php
                                            //http://www.w3schools.com/php/php_mysql_select.asp

                                            $stmt = $conn->prepare('SELECT Name, Address FROM `customer`');
                                            // execute query
                                            $stmt->execute();
                                            // Get the result
                                            $result = $stmt->get_result();

                                            if ($result->num_rows > 0) {
                                                // output data of each row
                                                while($row = $result->fetch_assoc()) {
                                                    echo "<option value=\"". $row["Name"]. "\"";
                                                    if ($row["Name"] === $name){
                                                        echo " selected";
                                                    }
                                                    echo ">". $row["Name"]." - ". $row["Address"] ."</option>";
                                                }
                                            } else {
                                                echo "<option value= \"0\"> 0 results</option>";}
                                            ?>
                                        </select>
                                        <label>Customer Name</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <select name="iname">

                                            <?php
                                            //http://www.w3schools.com/php/php_mysql_select.asp

                                            $stmt = $conn->prepare('SELECT itemname, itemdesc FROM `item`');
                                            // execute query
                                            $stmt->execute();
                                            // Get the result
                                            $result = $stmt->get_result();

                                            if ($result->num_rows > 0) {
                                                // output data of each row
                                                while($row = $result->fetch_assoc()) {
                                                    echo "<option value=\"". $row["itemname"]. "\"";
                                                    if ($row["itemname"] === $iname){
                                                        echo " selected";
                                                    }
                                                    echo ">". $row["itemname"]." - ". $row["itemdesc"] ."</option>";
                                                }
                                            } else {
                                                echo "<option value= \"0\"> 0 results</option>";}
                                            ?>
                                        </select>
                                        <label>Item Name</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <select name="vname">

                                            <?php
                                            //http://www.w3schools.com/php/php_mysql_select.asp

                                            $stmt = $conn->prepare('SELECT vname, vdesc FROM `vessel`');
                                            // execute query
                                            $stmt->execute();
                                            // Get the result
                                            $result = $stmt->get_result();

                                            if ($result->num_rows > 0) {
                                                // output data of each row
                                                while($row = $result->fetch_assoc()) {
                                                    echo "<option value=\"". $row["vname"]. "\"";
                                                    if ($row["vname"] === $vessel){
                                                        echo " selected";
                                                    }
                                                    echo ">". $row["vname"]." - ". $row["vdesc"] ."</option>";
                                                }
                                            } else {
                                                echo "<option value= \"0\"> 0 results</option>";}
                                            ?>
                                        </select>
                                        <label>Vessel Name</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input value="<?php echo $date?>" id="date" name="date" type="text" class="datepicker">
                                        <label for="date">Shipment Date</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12 right-align">
                                        <!--<input id="submit" name="submit" type="submit" class="waves-effect waves-light btn" value="Login">-->
                                        <button class="waves-effect waves-light btn" type="submit" name="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
include_once "footer.php"
?>