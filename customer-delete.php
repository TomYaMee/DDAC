<?php
/**
 * Created by PhpStorm.
 * User: TomYaMee
 * Date: 3/18/2018
 * Time: 12:55 PM
 */

include "config.php";
$stmt = $conn->prepare('DELETE FROM `Customer` WHERE id = ?');
$stmt->bind_param('i', $_GET['id']);

// Execute query
$stmt->execute();

// Get the result
$result = $stmt->get_result();

echo "<script>alert('Customer data deleted.'); window.location.replace('customer.php');</script>";