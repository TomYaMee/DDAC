<?php
/**
 * Created by PhpStorm.
 * User: TomYaMee
 * Date: 4/4/2018
 * Time: 1:55 PM
 */

include_once "header.php"
?>

    <br>
    <div class="container">
        <div class="row">
            <div class="col s8 offset-s2">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form id="loginForm" method="post" class="col s12" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="name" name="name" type="text" class="validate">
                                        <label for="name">Vessel Name</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="description" name="description" type="text" class="validate">
                                        <label for="description">Description</label>
                                    </div>
                                    <div class="row">
                                        <div class="col s12 right-align">
                                            <!--<input id="submit" name="submit" type="submit" class="waves-effect waves-light btn" value="Login">-->
                                            <button class="waves-effect waves-light btn" type="submit" name="submit">Submit</button>
                                        </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
include_once "footer.php"
?>

<?php
// Login
if(isset($_POST['submit']))
{
    if (!empty($_POST['name']) && !empty($_POST['description']))
    {
        $name = $_POST['name'];
        $desc = $_POST['description'];

        $stmt = $conn->prepare('Insert INTO vessel (vname, vdesc) VALUES (?, ?)');

        $stmt->bind_param('ss', $name,$desc);

        // execute query
        $stmt->execute();

        echo "<script>window.location.replace('vessel.php');</script>";

    }
    else
    {
        echo "<script>alert('Please fill in all empty fields.');";
        echo "window.location.replace('vessel.php');</script>";
    }
}