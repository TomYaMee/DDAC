<?php
/**
 * Created by PhpStorm.
 * User: TomYaMee
 * Date: 4/4/2018
 * Time: 2:36 PM
 */

include "config.php";
$stmt = $conn->prepare('DELETE FROM `shipment` WHERE sid = ?');
$stmt->bind_param('i', $_GET['id']);

// Execute query
$stmt->execute();

// Get the result
$result = $stmt->get_result();

echo "<script>alert('Shipment data deleted.'); window.location.replace('shipment.php');</script>";