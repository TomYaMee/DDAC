<?php
/**
 * Created by PhpStorm.
 * User: tommy
 * Date: 3/15/2018
 * Time: 1:47 PM
 */
include_once "header.php"
?>

    <div class="container">

        <br>
        <div class="row">
            <div class="col s3 offset-s9">
                <a href="agents-add.php" class="waves-effect waves-light btn"><i class="material-icons left">add</i>Create</a>
            </div>
        </div>

        <table class="responsive-table highlight">
            <thead>
            <tr>
                <th>Agent Name</th>
                <th>Username</th>
                <th>Email</th>
                <th>Actions</th>
            </tr>
            </thead>

            <tbody>
                <?php
                $sql2= "SELECT * FROM agent";
                $result = $conn->query($sql2);
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                        echo "<tr>";
                        echo "<td>" . $row["aname"] . "</td>";
                        echo "<td>" . $row["auser"] . "</td>";
                        echo "<td>" . $row["aemail"] . "</td>";
                        echo '<td> <a style="color: black" href="agents-edit.php?id=' . $row["auser"] . '"><i class="material-icons">edit</i></a><a style="color: black" href="agents-delete.php?id=' . $row["auser"] . '"> <i class="material-icons">delete</i></a> </td>';
                        echo "</tr>";
                    }
                };
                ?>
            </tbody>
        </table>
        <br>
        <br>
    </div>

<?php
include_once "footer.php"
?>