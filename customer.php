<?php
/**
 * Created by PhpStorm.
 * User: tommy
 * Date: 3/15/2018
 * Time: 1:57 PM
 */
include "logincheck.php";
include_once "header.php";

?>

<div class="container">

    <br>
    <div class="row">
        <div class="col s3 offset-s9">
            <a href="customer-add.php" class="waves-effect waves-light btn"><i class="material-icons left">add</i>Create</a>
        </div>
    </div>

    <table class="responsive-table highlight">
        <thead>
          <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Address</th>
              <th>Contact</th>
              <th>Actions</th>
          </tr>
        </thead>

        <tbody>
            <?php
            $sql2= "SELECT * FROM customer";
            $result = $conn->query($sql2);
            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    echo "<tr>";
                    echo "<td>" . $row["ID"] . "</td>";
                    echo "<td>" . $row["Name"] . "</td>";
                    echo "<td>" . $row["Address"] . "</td>";
                    echo "<td>" . $row["Contact"] . "</td>";
                    echo '<td> <a style="color: black" href="customer-edit.php?id=' . $row["ID"] . '"><i class="material-icons">edit</i></a><a style="color: black" href="customer-delete.php?id=' . $row["ID"] . '"> <i class="material-icons">delete</i></a> </td>';
                    echo "</tr>";
                }
            };
            ?>
        </tbody>
      </table>
    <br>
    <br>
</div>

<?php
include_once "footer.php"
?>