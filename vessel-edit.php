<?php
/**
 * Created by PhpStorm.
 * User: TomYaMee
 * Date: 4/4/2018
 * Time: 1:55 PM
 */

include_once "header.php";

if(isset($_POST['submit']))
{
    if (!empty($_POST['name']) && !empty($_POST['desc']))
    {
        $name = $_POST['name'];
        $desc = $_POST['desc'];
        $id = $_POST['id'];

        $stmt = $conn->prepare('UPDATE `vessel` SET `vname`= ?,`vdesc`= ? WHERE `vid` = ?');

        $stmt->bind_param('ssi', $name,$desc, $id);

        // execute query
        $stmt->execute();

        echo "<script>alert('Update successfully');window.location.replace('vessel.php');</script>";

    }
    else
    {
        echo "<script>alert('Please fill in all empty fields.');";
        echo "window.location.replace('vessel.php');</script>";
    }
}
else if(isset($_GET['id']))
{
    $id = $_GET['id'];
    $stmt = $conn->prepare('SELECT * FROM `vessel` WHERE `VID` = ?');

    $stmt->bind_param('i', $id);

    // execute query
    $stmt->execute();

    // Get the result
    $result = $stmt->get_result();
    $row = $result->fetch_assoc();

    if ($result->num_rows === 1)
    {
        $name = $row['vname'];
        $desc = $row['vdesc'];

    };
}
?>


    <br>
    <div class="container">
        <div class="row">
            <div class="col s8 offset-s2">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form id="loginForm" method="post" class="col s12" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                                <input type="hidden" name="id" id="id" value="<?php echo $id; ?>">

                                <div class="row">
                                    <div class="input-field col s12">
                                        <input value="<?php echo $name?>" id="name" name="name" type="text" class="validate">
                                        <label for="name">Vessel Name</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input value="<?php echo $desc?>" id="desc" name="desc" type="text" class="validate">
                                        <label for="desc">Description</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12 right-align">
                                        <!--<input id="submit" name="submit" type="submit" class="waves-effect waves-light btn" value="Login">-->
                                        <button class="waves-effect waves-light btn" type="submit" name="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
include_once "footer.php"
?>